const readline = require('readline');

const interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

interface.question(`
Pilih nomor:
1. Penambahan
2. Pengurangan
3. Perkalian
4. Pembagian
5. Kuadrat
6. Luas Persegi
7. Volume Kubus
8. Volume Tabung

nomor: `, function(dataDariUser){
    
    let num1,num2,result;
    

    if (dataDariUser == 1){
        console.log('No.1 Penambahan')
         interface.question('- Masukkan angka pertama: ', (num1) => {
            interface.question('- Masukkan angka kedua: ', (num2) =>{
                result = Number(num1) + Number(num2);
                console.log("Hasil penambahan " + num1 + " + " + num2 + " adalah = " + result)
                interface.close();
        });
    });
        
    }else if(dataDariUser == 2){
        console.log('No.2 Pengurangan')
        interface.question('- Masukkan angka pertama: ', (num1) => {
            interface.question('- Masukkan angka kedua: ', (num2) =>{
                result = Number(num1) - Number(num2);
                console.log("Hasil pengurangan " + num1 + " - " + num2 + " adalah = " + result)
                interface.close();
        });
    });
    }else if(dataDariUser == 3){
        console.log('No.3 Perkalian')
        interface.question('- Masukkan angka pertama: ', (num1) => {
            interface.question('- Masukkan angka kedua: ', (num2) =>{
                result = Number(num1) * Number(num2);
                console.log("Hasil perkalian " + num1 + " - " + num2 + " adalah = " + result)
                interface.close();
        });
    });
    }else if(dataDariUser == 4){
        console.log('No.4 Pembagian')
        interface.question('- Masukkan angka pertama: ', (num1) => {
            interface.question('- Masukkan angka kedua: ', (num2) =>{
                result = Number(num1) / Number(num2);
                console.log("Hasil perkalian " + num1 + " / " + num2 + " adalah = " + result)
                interface.close();
        });
    });
    }else if(dataDariUser == 5){
        console.log('No.5 Kuadrat')
        interface.question('- Masukkan angka pertama: ', (num1) => {
                result = Number(num1)**2 ;
                console.log("Hasil kuadrat " + num1+ " adalah = " + result)
                interface.close();
    });
    }else if(dataDariUser == 6){
        console.log('No.6 Luas Persegi')
        interface.question('- Masukkan sisi: ', (num1) => {
                result = Number(num1)**2 ;
                console.log("Luas persegi dengan sisi " + num1+ " adalah = " + result + 'cm²') 
                interface.close();
    });
    }else if(dataDariUser == 7){
        console.log('No.7 Volume Kubus')
        interface.question('- Masukkan panjang sisi: ', (num1) => {
                result = Number(num1) **3 ;
                console.log("Volume kubus dengan sisi " + num1+ " adalah = " + result + 'cm³')
                interface.close();
    });
    }else if(dataDariUser == 8){
        console.log('No.8 Volume Tabung')
        interface.question('- Masukkan radius lingkaran: ', (num1) => {
            interface.question('- Masukkan tinggi tabung : ', (num2) =>{
                result = Number(num1)**2 * Number(num2) * Math.PI;
                console.log("Volume tabung dengan radius " + num1 + "cm dan tinggi " + num2 + "cm adalah = " + result + 'cm')
                interface.close();
        });
    });
    }else{
        console.log("Salah, pilih angka 1-8.")
        interface.close();
    }
   
});



